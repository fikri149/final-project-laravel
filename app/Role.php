<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){
        return $this->belongsToMany('App\User',  'user_role', 'role_id', 'user_id');
    }

    public function routes(){
        return $this->belongsToMany('App\Route', 'route_role', 'role_id', 'route_id');
    }


}
