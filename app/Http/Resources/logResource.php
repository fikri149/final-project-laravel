<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class logResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'log_name' => $this->log_name,
    
        ];
    }
}
