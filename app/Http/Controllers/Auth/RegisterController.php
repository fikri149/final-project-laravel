<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'name' => ['alpha_num','required','unique:users,name'],
            'email' => ['email','required'],
            'password' => ['required','min:6'],
            'phone_number' => ['required','unique:users,phone_number']
        ]);

        //dd(request('phone_number'));

        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'phone_number' => request('phone_number'),
        ]);

        return response('Terimakasih anda sudah terdaftar!!');
    }
}
