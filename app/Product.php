<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'price', 'description'
    ];
    
    public function Order(){
        return $this->belongsToMany('App\Order',  'order_product', 'order_id', 'product_id');
    }
}
